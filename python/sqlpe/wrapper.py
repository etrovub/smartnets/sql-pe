import ctypes
from ctypes import (
    c_char_p,
    c_uint32,
    c_uint64,
)

lib = ctypes.cdll.LoadLibrary("libsqlpe.so")

class _ClibSqlPeStateMachine(ctypes.Structure):
    pass


class _ClibUnauthenticatedRole(ctypes.Structure):
    pass


class _ClibAuthenticatedRole(ctypes.Structure):
    pass


class SqlPeError(Exception):
    def __init__(self, rc):
        self.rc = rc

    def error_code(self):
        return self.rc


class UnauthenticatedRole:
    def __init__(self, _c_pointer):
        self._c_pointer = _c_pointer

    def __del___(self):
        if self._c_pointer:
            lib.sqlpe_unauthenticated_role_free(self._c_pointer)


class AuthenticatedRole:
    def __init__(self, _c_pointer):
        self._c_pointer = _c_pointer

    def __del___(self):
        if self._c_pointer:
            lib.sqlpe_authenticated_role_free(self._c_pointer)


class SqlPeStateMachine:
    def __init__(self):
        self._c_pointer = ctypes.POINTER(_ClibSqlPeStateMachine)()
        rc = lib.sqlpe_spsm_create(
            ctypes.byref(self._c_pointer),
        )
        process_result(rc)

    def __del__(self):
        if self._c_pointer:
            lib.sqlpe_spsm_free(self._c_pointer)

    def initialize(self, key: bytes):
        assert len(key) == 32
        _role_ptr = ctypes.POINTER(_ClibAuthenticatedRole)()
        rc = lib.sqlpe_spsm_init(self._c_pointer, ctypes.byref(_role_ptr), key)
        process_result(rc)

        AuthenticatedRole(_role_ptr)

    def validate_trust(self):
        rc = lib.sqlpe_spsm_validate_trust(self._c_pointer)
        process_result(rc)

    def create_role_with_password(self, name: str, password: bytes):
        rc = lib.sqlpe_spsm_create_role_with_password(self._c_pointer, name, password, len(password))
        process_result(rc)


def process_result(rc):
    if rc != 0:
        raise SqlPeError(rc)
