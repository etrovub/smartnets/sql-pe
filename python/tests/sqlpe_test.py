import pytest
import sqlpe

def test_create_spsm():
    spsm = sqlpe.SqlPeStateMachine()
    with pytest.raises(sqlpe.SqlPeError) as e:
        spsm.validate_trust()
    assert e.value.error_code() == -3

def test_init_spsm():
    spsm = sqlpe.SqlPeStateMachine()
    spsm.initialize(b"01234567890123456789012345678901")
    spsm.validate_trust()

def test_create_password():
    spsm = sqlpe.SqlPeStateMachine()
    spsm.initialize(b"01234567890123456789012345678901")
    spsm.create_role_with_password("alice", b"hunter2")
