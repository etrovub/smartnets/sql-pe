struct sqlpe_state_machine;
struct sqlpe_authenticated_role;
struct sqlpe_unauthenticated_role;

int sqlpe_spsm_create(struct sqlpe_state_machine **spsm_ptr);
int sqlpe_spsm_free(struct sqlpe_state_machine *spsm_ptr);
int sqlpe_spsm_init(struct sqlpe_state_machine *spsm_ptr,
                    struct sqlpe_authenticated_role **root,
                    const unsigned char *key);
int sqlpe_authenticated_role_free(struct sqlpe_authenticated_role *ptr);
int sqlpe_unauthenticated_role_free(struct sqlpe_unauthenticated_role *ptr);
int sqlpe_spsm_validate_trust(const struct sqlpe_state_machine *spsm_ptr);
