use std::ffi::{c_char, CStr};

use super::*;

/// Create a new role and set its password.
///
/// This calls [SqlPrivilegeStateMachine::create_role_with_password].
/// This requires the root user to be authenticated, and implies that the newly created user will
/// become authenticated as well.
///
/// # Safety
/// The supplied spsm pointer should point to the state machine as initialized by [sqlpe_spsm_create].
/// The name should be a valid null-terminated C string.  Additionally, the call will panic when
/// the name is not valid UTF-8.
#[no_mangle]
pub unsafe extern "C" fn sqlpe_spsm_create_role_with_password(
    spsm_ptr: *mut SqlPrivilegeStateMachine,
    name: *const c_char,
    password: *const u8,
    password_len: usize,
) -> c_int {
    let spsm = spsm_ptr.as_mut().expect("valid spsm");

    let name = CStr::from_ptr(name);
    let name = name.to_str().expect("utf8");

    let password: &[u8] = std::slice::from_raw_parts(password, password_len);

    try_c!(spsm.create_role_with_password(name, password));

    0
}
