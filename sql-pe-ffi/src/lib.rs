use std::{
    os::raw::{c_int, c_uchar},
    ptr::null_mut,
};

use sql_pe::{
    roles::{AuthenticatedRole, UnauthenticatedRole},
    SqlPeError, SqlPrivilegeStateMachine,
};

macro_rules! try_c {
    ($val:expr) => {
        match $val {
            Ok(x) => x,
            Err(SqlPeError::RootUnauthenticated) => return -2,
            Err(SqlPeError::RoleNotFound(_)) => return -3,
            Err(SqlPeError::RoleExists) => return -3,
            Err(SqlPeError::RoleWithoutSecret) => return -3,
            Err(SqlPeError::AlreadyAuthenticated) => return -4,
            Err(SqlPeError::AlreadyInitialized) => return -5,
            Err(SqlPeError::UnspecifiedCryptoError) => return -6,
            Err(SqlPeError::InconsistentTrust { reason: _ }) => return -7,
            Err(SqlPeError::MissingIdentityKey { role: _ }) => return -8,
            #[allow(unreachable_patterns)]
            Err(_) => return -1,
        }
    };
}

mod authentication;

/// Initialize an SQL-PE state machine.
///
/// This calls [SqlPrivilegeStateMachine::default()].
///
/// # Safety
/// The supplied pointer should point to valid memory.
#[no_mangle]
pub unsafe extern "C" fn sqlpe_spsm_create(spsm_ptr: *mut *mut SqlPrivilegeStateMachine) -> c_int {
    assert!(!spsm_ptr.is_null());
    // Initialize to null pointer
    *spsm_ptr = null_mut();

    // Create the actual SPSM
    let spsm = Box::new(SqlPrivilegeStateMachine::default());

    // Leak SPSM to C
    *spsm_ptr = Box::into_raw(spsm);

    // Return ok
    0
}

/// Free the SQL-PE state machine.
///
/// # Safety
/// The supplied pointer should point to the state machine as initialized by [sqlpe_spsm_create].
#[no_mangle]
pub unsafe extern "C" fn sqlpe_spsm_free(spsm_ptr: *mut SqlPrivilegeStateMachine) -> c_int {
    assert!(!spsm_ptr.is_null());

    // Take the SPSM from C
    let _spsm = Box::from_raw(spsm_ptr);

    // Return ok
    0
}

/// Initialize the root role in the SQL-PE state machine, with a 32-byte key.
///
/// This calls [SqlPrivilegeStateMachine::init].
/// If `root` is not NULL, it will be populated with a pointer to the newly authenticated root
/// user.  This must later be free'd with [sqlpe_authenticated_role_free].
///
/// # Safety
/// The supplied spsm pointer should point to the state machine as initialized by [sqlpe_spsm_create].
/// The key should point to a valid region of memory of 32 bytes.
#[no_mangle]
pub unsafe extern "C" fn sqlpe_spsm_init(
    spsm_ptr: *mut SqlPrivilegeStateMachine,
    root: *mut *mut AuthenticatedRole,
    key: *const c_uchar,
) -> c_int {
    let spsm = spsm_ptr.as_mut().expect("valid spsm");
    let secret = std::slice::from_raw_parts(key, 32);
    let role = try_c!(spsm.init(secret));
    if let Some(root) = root.as_mut() {
        *root = Box::leak(Box::new(role.clone()));
    }
    0
}

/// Free an authenticated role.
///
/// # Safety
/// The supplied pointer should point to the authenticated role as initialized by [sqlpe_spsm_init].
#[no_mangle]
pub unsafe extern "C" fn sqlpe_authenticated_role_free(ptr: *mut AuthenticatedRole) -> c_int {
    assert!(!ptr.is_null());

    // Take the Role from C
    let _role = Box::from_raw(ptr);

    // Return ok
    0
}

/// Free an unauthenticated role.
///
/// # Safety
/// The supplied pointer should point to the unauthenticated role.
#[no_mangle]
pub unsafe extern "C" fn sqlpe_unauthenticated_role_free(ptr: *mut UnauthenticatedRole) -> c_int {
    assert!(!ptr.is_null());

    // Take the Role from C
    let _role = Box::from_raw(ptr);

    // Return ok
    0
}

/// Validate the trust relations in the SQL-PE state machine.
///
/// This calls [SqlPrivilegeStateMachine::validate_trust()].
///
/// # Safety
/// The supplied pointer should point to the state machine as initialized by [sqlpe_spsm_create].
#[no_mangle]
pub unsafe extern "C" fn sqlpe_spsm_validate_trust(
    spsm_ptr: *const SqlPrivilegeStateMachine,
) -> c_int {
    let spsm = spsm_ptr.as_ref().expect("valid spsm");
    try_c!(spsm.validate_trust());
    0
}
