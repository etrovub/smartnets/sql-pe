#!/bin/sh -e

PROJECT_PATH=$(dirname "$(realpath "$0")")

cargo build --all
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${PROJECT_PATH}/target/debug
pytest -s -vvv python/tests
