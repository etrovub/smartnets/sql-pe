# Prototype SQL privilege encryption system for LumoSQL

[![doc](https://img.shields.io/badge/doc-public-blue)](https://etrovub.gitlab.io/smartnets/sql-pe/doc/sql_pe/)
[![doc-private](https://img.shields.io/badge/doc-dev-blue)](https://etrovub.gitlab.io/smartnets/sql-pe/doc-private/sql_pe/)
[![coverage report](https://gitlab.com/etrovub/smartnets/sql-pe/badges/main/coverage.svg)](https://gitlab.com/etrovub/smartnets/sql-pe/-/commits/main) 
