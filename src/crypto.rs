use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE,
    ristretto::{CompressedRistretto, RistrettoPoint},
    scalar::Scalar,
};
use digest::Digest;
use serde::*;
use sha3::Sha3_512;
use subtle::ConstantTimeEq;

use crate::SqlPeError;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct SchnorrSignature {
    r: CompressedRistretto,
    s: Scalar,
}

impl SchnorrSignature {
    /// Verifies a SchnorrSignature
    // XXX: Check whether it's in constant time.
    pub fn verify(&self, key: &RistrettoPoint, data: &[u8]) -> bool {
        // Digest the data
        let mut digest = Sha3_512::default();
        digest.update(self.r.as_bytes());
        digest.update(data);
        let digest = Scalar::from_hash(digest);

        let r_verify = &self.s * &RISTRETTO_BASEPOINT_TABLE + digest * key;
        self.r.ct_eq(&r_verify.compress()).into()
    }

    /// Generate Schnorr signature given a private key
    pub fn sign<R: rand::CryptoRng + rand::RngCore>(
        rng: &mut R,
        key: Scalar,
        data: &[u8],
    ) -> Result<SchnorrSignature, SqlPeError> {
        let k = Scalar::random(rng);
        let r = &k * &RISTRETTO_BASEPOINT_TABLE;
        let r = r.compress();

        let mut digest = Sha3_512::default();
        digest.update(r.as_bytes());
        digest.update(data);

        let e = Scalar::from_hash(digest);
        let s = k - key * e;
        Ok(SchnorrSignature { r, s })
    }

    pub fn recover_public_key<D: AsRef<[u8]>>(
        &self,
        data: D,
    ) -> Result<RistrettoPoint, SqlPeError> {
        let data = data.as_ref();
        let r = self
            .r
            .decompress()
            .ok_or(SqlPeError::UnspecifiedCryptoError)?;

        let mut digest = Sha3_512::default();
        digest.update(self.r.as_bytes());
        digest.update(data);
        let digest = Scalar::from_hash(digest);

        Ok(digest.invert() * (r - &self.s * &RISTRETTO_BASEPOINT_TABLE))
    }
}

#[derive(Clone, Debug)]
pub enum TrustRoot {
    NoTrust,
    Signed { signature: SchnorrSignature },
}

impl TrustRoot {
    pub fn recover_public_key<D: AsRef<[u8]>>(
        &self,
        data: D,
    ) -> Option<Result<CompressedRistretto, SqlPeError>> {
        match self {
            Self::NoTrust => None,
            Self::Signed { signature } => {
                Some(signature.recover_public_key(data).map(|x| x.compress()))
            }
        }
    }
}
