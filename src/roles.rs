mod authentication;

use authentication::*;
use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE,
    ristretto::{CompressedRistretto, RistrettoPoint},
};

use crate::crypto::{SchnorrSignature, TrustRoot};

use super::SqlPeError;

/// The [RoleManager] manages the key material for authentication purposes, and keeps track of the
/// currently authenticated role.
///
/// **Note:** The [RoleManager] is capable of maintaining unsigned roles (i.e., roles that have been created
/// without any other role endorsing it) and of trust chains.
/// Notably, it will not maintain any semantics about trust relations of roles.  Semantics of trust
/// and privilege relations should be maintained *outside* of the [RoleManager], for instance
/// through the [SqlPrivilegeStateMachine](crate::SqlPrivilegeStateMachine).
#[derive(Default)]
pub struct RoleManager {
    roles: Vec<UnauthenticatedRole>,
    authenticated_role: Vec<AuthenticatedRole>,
}

impl RoleManager {
    pub fn create_role(&mut self, name: &str) -> Result<&UnauthenticatedRole, SqlPeError> {
        match self.by_name(name) {
            Ok(_) => return Err(SqlPeError::RoleExists),
            Err(SqlPeError::RoleNotFound(_)) => (),
            Err(e) => return Err(e),
        }
        self.roles.push(UnauthenticatedRole {
            name: name.into(),
            identity_key: None,
            secret: None,
            root: TrustRoot::NoTrust,
            authentication_methods: Default::default(),
        });
        Ok(&self.roles[self.roles.len() - 1])
    }

    pub fn iter(&self) -> &[UnauthenticatedRole] {
        &self.roles
    }

    pub fn sign_role(&mut self, signer: &str, name: &str) -> Result<(), SqlPeError> {
        // XXX Should this also sign the AuthenticationMethods?
        let signer = self
            .authenticated_role
            .iter_mut()
            .find(|x| x.name == signer)
            .ok_or(SqlPeError::RootUnauthenticated)?;
        let secret = signer.secret.identity_key;

        let role_to_sign = self.by_name_mut(name)?;

        let signature = SchnorrSignature::sign(
            &mut rand::thread_rng(),
            secret,
            &role_to_sign.additional_authenticated_data(),
        )?;

        role_to_sign.root = TrustRoot::Signed { signature };
        Ok(())
    }

    pub fn by_name(&self, name: &str) -> Result<&UnauthenticatedRole, SqlPeError> {
        self.roles
            .iter()
            .find(|x| x.name == name)
            .ok_or_else(|| SqlPeError::RoleNotFound(name.to_string()))
    }

    fn by_name_mut(&mut self, name: &str) -> Result<&mut UnauthenticatedRole, SqlPeError> {
        self.roles
            .iter_mut()
            .find(|x| x.name == name)
            .ok_or_else(|| SqlPeError::RoleNotFound(name.to_string()))
    }

    pub fn is_authenticated(&self, name: &str) -> bool {
        self.authenticated_role.iter().any(|x| x.name == name)
    }

    pub fn authenticated_by_name(&self, name: &str) -> Result<&AuthenticatedRole, SqlPeError> {
        self.authenticated_role
            .iter()
            .find(|x| x.name == name)
            .ok_or_else(|| SqlPeError::RoleNotFound(name.to_string()))
    }
}

#[derive(Clone, Debug)]
pub struct UnauthenticatedRole {
    pub name: String,
    pub identity_key: Option<CompressedRistretto>,
    pub secret: Option<EncryptedSecret>,
    pub authentication_methods: AuthenticationMethods,
    pub root: TrustRoot,
}

#[derive(Clone, Debug)]
pub struct AuthenticatedRole {
    pub name: String,
    pub secret: PlaintextSecret,
    pub secret_encryption_key: [u8; 32],
}

impl AuthenticatedRole {
    pub fn identity_key(&self) -> CompressedRistretto {
        (&self.secret.identity_key * &RISTRETTO_BASEPOINT_TABLE).compress()
    }
}

impl UnauthenticatedRole {
    pub fn signer_key(&self) -> Option<Result<CompressedRistretto, SqlPeError>> {
        self.root
            .recover_public_key(self.additional_authenticated_data())
    }

    pub fn verify_trust(&self, key: &RistrettoPoint) -> Option<bool> {
        match &self.root {
            TrustRoot::NoTrust => None,
            TrustRoot::Signed { signature } => {
                Some(signature.verify(key, &self.additional_authenticated_data()))
            }
        }
    }
}
