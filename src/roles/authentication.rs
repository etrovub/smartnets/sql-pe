use std::collections::HashMap;

use chacha20poly1305::aead::{AeadInPlace, NewAead};
// Or `XChaCha20Poly1305`
use chacha20poly1305::{ChaCha20Poly1305, Key, Nonce};
use curve25519_dalek::constants::RISTRETTO_BASEPOINT_TABLE;
use curve25519_dalek::scalar::Scalar;
use serde::*;

use super::*;

mod password;
pub use password::*;

pub type EncryptedSecret = Vec<u8>;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct PlaintextSecret {
    pub(crate) identity_key: Scalar,
    #[serde(flatten)]
    pub(crate) inner: HashMap<String, Vec<u8>>,
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct AuthenticationMethods {
    password: Option<PasswordAuthenticationMethod>,
}

impl PlaintextSecret {
    pub fn generate<R: rand::CryptoRng + rand::RngCore>(rng: &mut R) -> Self {
        Self {
            identity_key: Scalar::random(rng),
            inner: Default::default(),
        }
    }
}

impl RoleManager {
    /// Deauthenticates the authenticated role, by name, if any.
    ///
    /// Returns whether there was a role to deauthenticate.
    pub fn deauthenticate(&mut self, name: &str) -> bool {
        if let Some((i, _)) = self
            .authenticated_role
            .iter()
            .enumerate()
            .find(|(_, x)| x.name == name)
        {
            self.authenticated_role.remove(i);
            true
        } else {
            false
        }
    }

    /// Tries to authenticate a role, given the role's 32-byte key.
    pub fn authenticate_by_key(
        &mut self,
        name: &str,
        key: &[u8],
    ) -> Result<&AuthenticatedRole, SqlPeError> {
        if self.is_authenticated(name) {
            return Err(SqlPeError::AlreadyAuthenticated);
        }
        assert_eq!(key.len(), 32);

        let role = self.by_name(name)?;

        let mut secret = role
            .secret
            .as_ref()
            .ok_or(SqlPeError::RoleWithoutSecret)?
            // XXX these Vec allocations are unnecessary. Ring does this better.
            .to_vec();
        let key = Key::from_slice(key);
        let nonce = Nonce::default();
        let cipher = ChaCha20Poly1305::new(key);
        cipher.decrypt_in_place(&nonce, &role.additional_authenticated_data(), &mut secret)?;

        // XXX: this should be returned as an error
        let secret: PlaintextSecret = serde_json::from_slice(&secret).expect("deserializable");

        assert_eq!(
            &(&secret.identity_key * &RISTRETTO_BASEPOINT_TABLE).compress(),
            role.identity_key
                .as_ref()
                .expect("public identity key present")
        );

        self.authenticated_role.push(AuthenticatedRole {
            name: role.name.clone(),
            secret,
            secret_encryption_key: *AsRef::<[u8; 32]>::as_ref(key),
        });

        Ok(self.authenticated_role.last().unwrap())
    }

    /// Sets a role's authentication key to the 32-byte supplied `key`.
    ///
    /// Returns `Err(SqlPeError::AlreadyAuthenticated)` when trying to set the authentication key
    /// on a currently authenticated role.
    ///
    /// Returns `Ok(&AuthenticatedRole)`, a reference to the newly authenticated role.
    pub fn set_authentication_key(
        &mut self,
        name: &str,
        key: &[u8],
    ) -> Result<&AuthenticatedRole, SqlPeError> {
        if self.is_authenticated(name) {
            return Err(SqlPeError::AlreadyAuthenticated);
        }
        assert_eq!(key.len(), 32);

        let role = self.by_name_mut(name)?;

        let secret = PlaintextSecret::generate(&mut rand::thread_rng());
        let mut secret_serialized: Vec<u8> =
            serde_json::to_vec(&secret).expect("empty secret serializable");
        role.identity_key = Some((&secret.identity_key * &RISTRETTO_BASEPOINT_TABLE).compress());

        let key = Key::from_slice(key);

        let authenticated_role = AuthenticatedRole {
            name: role.name.clone(),
            secret,
            secret_encryption_key: *AsRef::<[u8; 32]>::as_ref(key),
        };

        let nonce = Nonce::default();
        let cipher = ChaCha20Poly1305::new(key);
        cipher.encrypt_in_place(
            &nonce,
            &role.additional_authenticated_data(),
            &mut secret_serialized,
        )?;
        role.secret = Some(secret_serialized);

        self.authenticated_role.push(authenticated_role);
        Ok(self.authenticated_role.last().unwrap())
    }
}

impl UnauthenticatedRole {
    pub(crate) fn additional_authenticated_data(&self) -> Vec<u8> {
        // TODO: document and standardize format of the AD, and take out the bugs.
        let mut aad = Vec::new();
        aad.extend(
            self.identity_key
                .as_ref()
                .map(CompressedRistretto::as_bytes)
                .map(|x| x as &[u8])
                .unwrap_or(&[]),
        );
        aad.extend(self.name.as_bytes());

        aad
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_user_and_set_key_and_reauthenticate() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        rm.set_authentication_key("root", &[0u8; 32])
            .expect("not yet authenticated");
        assert!(rm.is_authenticated("root"));

        assert!(rm.deauthenticate("root"));
        rm.authenticate_by_key("root", &[0u8; 32])
            .expect("authenticate root user");
    }

    #[test]
    fn create_user_and_set_key_and_reauthenticate_without_deauthenticate() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        rm.set_authentication_key("root", &[0u8; 32])
            .expect("not yet authenticated");
        assert!(rm.is_authenticated("root"));

        // assert!(rm.deauthenticate("root"));
        assert!(matches!(
            rm.authenticate_by_key("root", &[0u8; 32]),
            Err(SqlPeError::AlreadyAuthenticated)
        ));
        assert!(rm.is_authenticated("root"));
    }

    #[test]
    fn create_user_and_set_key_and_reauthenticate_with_wrong_key() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        rm.set_authentication_key("root", &[0u8; 32])
            .expect("not yet authenticated");
        assert!(rm.is_authenticated("root"));

        assert!(rm.deauthenticate("root"));
        assert!(matches!(
            rm.authenticate_by_key("root", &[1u8; 32]),
            Err(SqlPeError::UnspecifiedCryptoError)
        ));
        assert!(!rm.is_authenticated("root"));
    }

    #[test]
    fn create_user_and_set_key_and_rename_and_reauthenticate() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        rm.set_authentication_key("root", &[0u8; 32])
            .expect("not yet authenticated");
        assert!(rm.is_authenticated("root"));

        assert!(rm.deauthenticate("root"));
        rm.roles[0].name = "toor".into();

        // This should fail on the additional authenticated data.
        assert!(matches!(
            rm.authenticate_by_key("toor", &[1u8; 32]),
            Err(SqlPeError::UnspecifiedCryptoError)
        ));
        assert!(!rm.is_authenticated("root"));
    }

    #[test]
    fn create_user_and_authenticate_panics() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        assert!(matches!(
            rm.authenticate_by_key("root", &[0u8; 32]),
            Err(SqlPeError::RoleWithoutSecret)
        ));
        assert!(!rm.is_authenticated("root"));
    }
}
