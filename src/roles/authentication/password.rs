use crate::SqlPeError;
use argon2::{Argon2, ParamsBuilder};
use rand::{Rng, RngCore};
use serde::{Deserialize, Serialize};

use super::*;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct PasswordAuthenticationMethod {
    salt: Vec<u8>,
    version: u32,
    m_cost: u32,
    t_cost: u32,
    p_cost: u32,
    encrypted_secret_encryption_key: Vec<u8>,
}

impl RoleManager {
    pub fn authenticate_by_password(
        &mut self,
        name: &str,
        password: impl AsRef<[u8]>,
    ) -> Result<&AuthenticatedRole, SqlPeError> {
        if self.is_authenticated(name) {
            return Err(SqlPeError::AlreadyAuthenticated);
        }

        let role = self.by_name(name)?;

        // 1. Fetch the secret encryption key from the authenticated user
        let auth = role
            .authentication_methods
            .password
            .as_ref()
            .ok_or(SqlPeError::RoleWithoutPassword)?;
        let mut sek = auth.encrypted_secret_encryption_key.clone();

        // 2. Hash password
        let mut params = ParamsBuilder::new();
        params
            .m_cost(auth.m_cost)
            .map_err(|_| SqlPeError::UnspecifiedCryptoError)?
            .t_cost(auth.t_cost)
            .map_err(|_| SqlPeError::UnspecifiedCryptoError)?
            .p_cost(auth.p_cost)
            .map_err(|_| SqlPeError::UnspecifiedCryptoError)?;
        let argon2 = Argon2::new(
            argon2::Algorithm::Argon2id,
            argon2::Version::try_from(auth.version)
                .map_err(|_| SqlPeError::UnspecifiedCryptoError)?,
            params
                .params()
                .map_err(|_| SqlPeError::UnspecifiedCryptoError)?,
        );
        let mut hash = [0; 32];
        argon2
            .hash_password_into(password.as_ref(), &auth.salt, &mut hash)
            .map_err(|_| SqlPeError::UnspecifiedCryptoError)?;

        // 3. Use hash to decrypt the secret encryption key

        let key = Key::from_slice(&hash);
        let nonce = Nonce::default();
        let cipher = ChaCha20Poly1305::new(key);
        cipher.decrypt_in_place(&nonce, &[], &mut sek)?;

        self.authenticate_by_key(name, &sek)
    }

    /// Sets the authenticated role's authentication password to the supplied `password`.
    pub fn set_password(
        &mut self,
        name: &str,
        password: impl AsRef<[u8]>,
    ) -> Result<bool, SqlPeError> {
        let role = match self.authenticated_by_name(name) {
            Ok(role) => role,
            Err(SqlPeError::RoleNotFound(_)) => {
                let mut key = [0u8; 32];
                rand::thread_rng().fill_bytes(&mut key);
                self.set_authentication_key(name, &key)?
            }
            Err(e) => return Err(e),
        };

        // 1. Hash password
        let argon2 = Argon2::default();
        let salt: [u8; 32] = rand::thread_rng().gen();
        let mut hash = [0; 32];
        argon2
            .hash_password_into(password.as_ref(), &salt, &mut hash)
            .map_err(|_| SqlPeError::UnspecifiedCryptoError)?;

        // 2. Use hash to encrypt the secret encryption key
        // 2.1 Fetch the secret encryption key from the authenticated user
        let mut sek = role.secret_encryption_key.to_vec();

        // 2.2 Encrypt the secret encryption key with the password hash
        let key = Key::from_slice(&hash);
        let nonce = Nonce::default();
        let cipher = ChaCha20Poly1305::new(key);
        cipher.encrypt_in_place(&nonce, &[], &mut sek)?;

        let pam = PasswordAuthenticationMethod {
            salt: salt.into(),
            version: argon2::Version::default().into(),
            m_cost: argon2.params().m_cost(),
            t_cost: argon2.params().t_cost(),
            p_cost: argon2.params().p_cost(),
            encrypted_secret_encryption_key: sek,
        };

        // 3. Set the password authentication method
        let prev = self
            .by_name_mut(name)?
            .authentication_methods
            .password
            .replace(pam);

        Ok(prev.is_some())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_user_and_set_password_and_reauthenticate() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        rm.set_password("root", "hunter2")
            .expect("set authentication key");
        assert!(rm.is_authenticated("root"));

        assert!(rm.deauthenticate("root"));
        rm.authenticate_by_password("root", "hunter2")
            .expect("authenticate root user");
    }

    #[test]
    fn create_user_and_set_password_and_reauthenticate_without_deauthenticate() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        rm.set_password("root", "hunter2")
            .expect("set authentication key");
        assert!(rm.is_authenticated("root"));

        // assert!(rm.deauthenticate());
        assert!(matches!(
            rm.authenticate_by_password("root", "hunter2"),
            Err(SqlPeError::AlreadyAuthenticated)
        ));
        assert!(rm.is_authenticated("root"));
    }

    #[test]
    fn create_user_and_set_password_and_reauthenticate_with_wrong_password() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        rm.set_password("root", "hunter2")
            .expect("set authentication key");
        assert!(rm.is_authenticated("root"));

        assert!(rm.deauthenticate("root"));
        assert!(matches!(
            rm.authenticate_by_password("root", "hunter22"),
            Err(SqlPeError::UnspecifiedCryptoError)
        ));
        assert!(!rm.is_authenticated("root"));
    }

    #[test]
    fn create_user_and_set_password_and_rename_and_reauthenticate() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        rm.set_password("root", "hunter2")
            .expect("set authentication key");
        assert!(rm.is_authenticated("root"));

        assert!(rm.deauthenticate("root"));
        rm.roles[0].name = "toor".into();

        // This should fail on the additional authenticated data.
        assert!(matches!(
            rm.authenticate_by_password("toor", "hunter2"),
            Err(SqlPeError::UnspecifiedCryptoError)
        ));
        assert!(!rm.is_authenticated("root"));
    }

    #[test]
    fn create_user_and_authenticate_panics() {
        let mut rm = RoleManager::default();
        rm.create_role("root").expect("create root");
        assert!(!rm.is_authenticated("root"));
        assert!(!rm.deauthenticate("root"));

        assert!(matches!(
            rm.authenticate_by_password("root", "foobar"),
            Err(SqlPeError::RoleWithoutPassword)
        ));
        assert!(!rm.is_authenticated("root"));
    }
}
