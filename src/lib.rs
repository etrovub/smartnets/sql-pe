//! # SQL-PE: encrypted privilege subsystem for relational databases
//!
//! SQL-PE is a predicate encryption subsystem for SQL databases.  SQL-PE is mainly targetted as a
//! module for [LumoSQL](https://lumosql.org/), where it functions as the main privilege subsystem.
//!
//! In short: SQL-PE offers fine-grained privileges to SQL databases by encrypting database rows,
//! tables, column, or the database as a whole.
//!
//! This library consists of these main parts that work together:
//! - The authentication subsystem
//! - The privilege management subsystem
//! - The encryption subsystem
//!
//! The database engine will call this library through the [SqlPrivilegeStateMachine], for
//! authenticating users, for declaring privileges, and for requesting the encryption or decryption
//! of a database object.
//!
//! ## Design decisions
//!
//! When designing a system like SQL-PE, several system parameters need to be fixed.  While we
//! aimed to keep as much flexibility of possible, some decisions have been taken.
//!
//! ### Roles and role membership is not secret information
//!
//! While there can be explicit support for keeping roles secret in the future, SQL-PE currently
//! has a public listing of roles and their relations.
//!
//! ### Tables and privileges are not secret information
//!
//! Like roles, the existance of tables and their schema is public knowledge, as are their
//! relations with roles.
//!
//! ## Example
//!
//! ```rust
//! # use sql_pe::*;
//! use rand::Rng;
//! let mut spsm = SqlPrivilegeStateMachine::default();
//! let root_secret: [u8; 32] = rand::thread_rng().gen();
//! let root = spsm.init(&root_secret).expect("clean SPSM");
//! ```

/// Cryptographic primitives
pub(crate) mod crypto;
/// Storage structures for privilege data
pub mod privileges;
/// Storage structures for role data
pub mod roles;

mod state_machine;

use privileges::*;
use roles::*;

pub use state_machine::*;
