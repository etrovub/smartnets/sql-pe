use super::*;

/// Main state machine of the SQL-PE library.
///
/// Under the hood, this abstracts over [RoleManager] and [PrivilegeManager], and maintains the
/// semantics of roles and privileges, and keeps the relations between roles and privileges
/// consistent.
#[derive(Default)]
pub struct SqlPrivilegeStateMachine {
    roles: RoleManager,
    privileges: PrivilegeManager,
}

#[derive(thiserror::Error, Debug)]
pub enum SqlPeError {
    /// The root user should be authenticated for this operation
    #[error("root unauthenticated")]
    RootUnauthenticated,
    /// Role not found
    #[error("no such role {0}")]
    RoleNotFound(String),
    /// Role already exists
    #[error("cannot create a role that already exists")]
    RoleExists,
    /// The requested role does not have an associated secret.
    #[error("role without secret")]
    RoleWithoutSecret,
    /// The requested role does not have an associated password authentication method.
    #[error("role without password authentication method")]
    RoleWithoutPassword,

    /// Another role is already authenicated
    #[error("another role is authenicated")]
    AlreadyAuthenticated,

    /// Privilege system is already authenicated
    #[error("privilege system already initialized")]
    AlreadyInitialized,

    /// Unspecified cryptographic error
    ///
    /// This is deliberately left opaque to prevent side-channel leakage.
    #[error("cryptographic error")]
    UnspecifiedCryptoError,

    /// Trust subsystem is inconsistent
    #[error("inconistent trust subsystem: {reason}")]
    InconsistentTrust { reason: String },
    /// The requested operation requires an unavailable identity key
    #[error("identity key required, but unavailable for role `{role}'")]
    MissingIdentityKey { role: String },
}

impl From<aead::Error> for SqlPeError {
    fn from(_: aead::Error) -> Self {
        SqlPeError::UnspecifiedCryptoError
    }
}

impl SqlPrivilegeStateMachine {
    /// Creates the root user and its key information.
    ///
    /// Returns the authenticated root user.
    pub fn init(&mut self, root_secret: &[u8]) -> Result<&AuthenticatedRole, SqlPeError> {
        // 1. Create root user
        let _root = self.roles.create_role("root").map_err(|r| match r {
            SqlPeError::RoleExists => SqlPeError::AlreadyInitialized,
            r => r,
        })?;
        self.roles.set_authentication_key("root", root_secret)?;
        self.roles.sign_role("root", "root")?;

        if cfg!(debug_assertions) {
            self.validate_trust()
                .expect("trust relations initialized correctly");
        }

        self.authenticated_root()
    }

    /// Create a new role and set its password.
    ///
    /// Root needs to be authenticated for this call to succeed.
    pub fn create_role_with_password(
        &mut self,
        name: &str,
        password: impl AsRef<[u8]>,
    ) -> Result<&UnauthenticatedRole, SqlPeError> {
        // Fetch root to assert we are authenticated as root
        let _root = self.authenticated_root()?;
        // Create the role without the secret
        let _role = self.roles.create_role(name)?;
        // Set the role's secret, returning no new authenticated role.
        let password_changed = self.roles.set_password(name, password)?;
        assert!(!password_changed, "creating a role starts from no password");
        // Have root sign the new role
        self.roles.sign_role("root", name)?;
        // Validate trust system in debug mode.
        if cfg!(debug_assertions) {
            self.validate_trust()
                .expect("trust relations initialized correctly");
        }
        // Finally return the newly unauthenticated role.
        self.role(name)
    }

    pub fn create_role(
        &mut self,
        name: &str,
        secret: &[u8],
    ) -> Result<&UnauthenticatedRole, SqlPeError> {
        // Fetch root to assert we are authenticated as root
        let _root = self.authenticated_root()?;
        // Create the role without the secret
        let _role = self.roles.create_role(name)?;
        // Set the role's secret, returning no new authenticated role.
        let _new_auth = self.roles.set_authentication_key(name, secret)?;
        // Have root sign the new role
        self.roles.sign_role("root", name)?;
        // Validate trust system in debug mode.
        if cfg!(debug_assertions) {
            self.validate_trust()
                .expect("trust relations initialized correctly");
        }
        // Finally return the newly unauthenticated role.
        self.role(name)
    }

    /// Validates the trust relations in the privilege subsystem.
    ///
    /// **Note:**
    /// In principe these relations are enforced throughout the SQL-PE implementation, and this
    /// method needs not be called.  It is mainly intended as debugging aid.
    pub fn validate_trust(&self) -> Result<(), SqlPeError> {
        // 1. Every role is signed by root, including root itself.
        let root = self.role("root")?;
        let root_identity_key = root.identity_key.ok_or(SqlPeError::RoleWithoutSecret)?;
        let root_signer_key = root.signer_key().ok_or(SqlPeError::InconsistentTrust {
            reason: "no signer key for root".into(),
        })??;
        if root_signer_key != root_identity_key {
            return Err(SqlPeError::InconsistentTrust {
                reason: "root is not self-signed".into(),
            });
        }

        let root_secret = root_identity_key.decompress().unwrap();

        for role in self.roles.iter() {
            role.verify_trust(&root_secret)
                .ok_or_else(|| SqlPeError::InconsistentTrust {
                    reason: "unsigned role".into(),
                })?
                .then_some(())
                .ok_or_else(|| SqlPeError::InconsistentTrust {
                    reason: "invalid root signature on role".into(),
                })?;
        }

        for grant in self.privileges.iter() {
            grant
                .verify_trust(&root_secret)
                .ok_or_else(|| SqlPeError::InconsistentTrust {
                    reason: "unsigned grant".into(),
                })?
                .then_some(())
                .ok_or_else(|| SqlPeError::InconsistentTrust {
                    reason: "invalid root signature on grant".into(),
                })?;
        }

        Ok(())
    }

    pub fn roles(&self) -> &RoleManager {
        &self.roles
    }

    pub fn privileges(&self) -> &PrivilegeManager {
        &self.privileges
    }

    pub fn grant(&mut self, whom: &str, what: Privilege, on: Object) -> Result<&Grant, SqlPeError> {
        let whom = self.roles.by_name(whom)?;
        self.privileges
            .grant(self.roles.authenticated_by_name("root")?, whom, what, on)
    }

    pub fn role(&self, name: &str) -> Result<&UnauthenticatedRole, SqlPeError> {
        self.roles.by_name(name)
    }

    pub fn authenticated_root(&self) -> Result<&AuthenticatedRole, SqlPeError> {
        self.roles
            .authenticated_by_name("root")
            .map_err(|x| match x {
                SqlPeError::RoleNotFound(name) if &name == "root" => {
                    SqlPeError::RootUnauthenticated
                }
                e => e,
            })
    }

    pub fn deauthenticate(&mut self, name: &str) -> bool {
        self.roles.deauthenticate(name)
    }

    pub fn authenticate_by_key(
        &mut self,
        name: &str,
        key: &[u8],
    ) -> Result<&AuthenticatedRole, SqlPeError> {
        self.roles.authenticate_by_key(name, key)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::Rng;

    #[test]
    fn reinit_should_fail() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        assert!(matches!(spsm.init(&root_secret), Err(_)));
    }

    #[test]
    fn unauthenticated_role_creation_fails_validation() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        assert!(spsm.deauthenticate("root"));
        let _alice = spsm
            .roles
            .create_role("alice")
            .expect("create `alice' role");
        assert!(matches!(
            spsm.validate_trust(),
            Err(SqlPeError::InconsistentTrust { reason: _ })
        ));
    }

    #[test]
    fn role_creation_with_password_succeeds() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        spsm.create_role_with_password("alice", "hunter2")
            .expect("created `alice' role");
        assert!(matches!(spsm.validate_trust(), Ok(())));
    }

    #[test]
    fn role_creation_succeeds() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        let alice_secret: [u8; 32] = rand::thread_rng().gen();
        spsm.create_role("alice", &alice_secret)
            .expect("created `alice' role");
        assert!(matches!(spsm.validate_trust(), Ok(())));
    }

    #[test]
    fn multiple_role_creation_succeeds() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        let alice_secret: [u8; 32] = rand::thread_rng().gen();
        let bob_secret: [u8; 32] = rand::thread_rng().gen();
        let carol_secret: [u8; 32] = rand::thread_rng().gen();
        spsm.create_role("alice", &alice_secret)
            .expect("created `alice' role");
        spsm.create_role("bob", &bob_secret)
            .expect("created `bob' role");
        assert!(matches!(spsm.validate_trust(), Ok(())));

        // Let's swap Alice and root around, for fun.
        spsm.deauthenticate("root");
        spsm.authenticate_by_key("root", &root_secret)
            .expect("reauthenticate root");
        spsm.create_role("carol", &carol_secret)
            .expect("created `carol' role");
        assert!(matches!(spsm.validate_trust(), Ok(())));
    }

    #[test]
    fn unauthenticated_role_creation_unauthenticated_error() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        let alice_secret: [u8; 32] = rand::thread_rng().gen();
        assert!(spsm.deauthenticate("root"));
        assert!(matches!(
            dbg!(spsm.create_role("alice", &alice_secret)),
            Err(SqlPeError::RootUnauthenticated)
        ));
        assert!(matches!(spsm.validate_trust(), Ok(())));
    }

    #[test]
    fn create_role_as_non_root() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        let alice_secret: [u8; 32] = rand::thread_rng().gen();
        let bob_secret: [u8; 32] = rand::thread_rng().gen();
        spsm.create_role("alice", &alice_secret)
            .expect("created `alice' role");

        assert!(spsm.deauthenticate("alice"));
        spsm.authenticate_by_key("alice", &alice_secret)
            .expect("authenticate alice");

        assert!(spsm.deauthenticate("root"));
        assert!(matches!(
            dbg!(spsm.create_role("bob", &bob_secret)),
            Err(SqlPeError::RootUnauthenticated)
        ));
    }

    #[test]
    fn inject_role_as_non_root_fails_validation() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        let alice_secret: [u8; 32] = rand::thread_rng().gen();
        let bob_secret: [u8; 32] = rand::thread_rng().gen();
        spsm.create_role("alice", &alice_secret)
            .expect("created `alice' role");

        assert!(spsm.deauthenticate("root"));

        // Create the role without the secret
        let _role = spsm.roles.create_role("bob").expect("create `bob' role");
        // Set the role's secret, returning no new authenticated role.
        spsm.roles
            .set_authentication_key("bob", &bob_secret)
            .expect("set `bob' key");
        // Have **alice** sign the new role, which is disallowed by the SPSM
        spsm.roles
            .sign_role("alice", "bob")
            .expect("let `alice' sign `bob' role");
        assert!(matches!(
            spsm.validate_trust(),
            Err(SqlPeError::InconsistentTrust { reason: _ })
        ))
    }

    #[test]
    fn grant_succeeds() {
        let mut spsm = SqlPrivilegeStateMachine::default();
        let root_secret: [u8; 32] = rand::thread_rng().gen();
        let _root = spsm.init(&root_secret).expect("clean SPSM");

        let alice_secret: [u8; 32] = rand::thread_rng().gen();
        spsm.create_role("alice", &alice_secret)
            .expect("created `alice' role");

        spsm.grant(
            "alice",
            Privilege::Insert,
            Object::Table {
                name: "pictures".into(),
            },
        )
        .expect("grant alice privilege on pictures");

        assert!(matches!(spsm.validate_trust(), Ok(())));
    }
}
