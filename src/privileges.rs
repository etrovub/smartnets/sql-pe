use curve25519_dalek::ristretto::{CompressedRistretto, RistrettoPoint};

use crate::{
    crypto::{SchnorrSignature, TrustRoot},
    roles::{AuthenticatedRole, UnauthenticatedRole},
    SqlPeError,
};

/// The [PrivilegeManager] maintains the table-role and role-subrole relations
#[derive(Default, Debug)]
pub struct PrivilegeManager {
    grants: Vec<Grant>,
}

#[derive(Clone, Debug)]
pub enum Object {
    Table { name: String },
    Column { table: String, column: String },
}

#[derive(Clone, Debug)]
pub struct Grant {
    whom: CompressedRistretto,
    what: Privilege,
    on: Object,

    signature: TrustRoot,
}

impl Grant {
    /// Returns the granter's public key if this GRANT has been signed.
    pub fn who(&self) -> Result<CompressedRistretto, SqlPeError> {
        self.signature
            .recover_public_key(self.authenticated_data())
            .ok_or(SqlPeError::InconsistentTrust {
                reason: "unsigned GRANT".into(),
            })?
    }

    fn authenticated_data(&self) -> Vec<u8> {
        let mut data = Vec::new();
        // data.extend(self.who.as_bytes());
        data.extend(self.whom.as_bytes());
        data.push(self.what.as_byte());

        match &self.on {
            Object::Table { name } => {
                data.push(b't');
                data.extend(name.as_bytes());
            }
            Object::Column { table, column } => {
                data.push(b'c');
                // TODO: concatenation vulnerability
                data.extend(table.as_bytes());
                data.extend(column.as_bytes());
            }
        }
        data
    }

    pub fn sign(&mut self, who: &AuthenticatedRole) -> Result<(), SqlPeError> {
        let signature = SchnorrSignature::sign(
            &mut rand::thread_rng(),
            who.secret.identity_key,
            &self.authenticated_data(),
        )?;

        self.signature = TrustRoot::Signed { signature };
        Ok(())
    }

    pub fn verify_trust(&self, key: &RistrettoPoint) -> Option<bool> {
        match &self.signature {
            TrustRoot::NoTrust => None,
            TrustRoot::Signed { signature } => {
                Some(signature.verify(key, &self.authenticated_data()))
            }
        }
    }

    /// Whether the `self` grant implies the `what` on object `on`.
    pub fn implies(&self, what: &Privilege, on: &Object) -> bool {
        if self.what != *what {
            return false;
        }
        match (&self.on, on) {
            (Object::Table { name: n1 }, Object::Table { name: n2 }) => n1 == n2,
            // Column never implies table access
            (Object::Column { .. }, Object::Table { .. }) => false,
            (Object::Table { name: n1 }, Object::Column { table: n2, .. }) => n1 == n2,
            (
                Object::Column {
                    table: n1,
                    column: c1,
                },
                Object::Column {
                    table: n2,
                    column: c2,
                },
            ) => n1 == n2 && c1 == c2,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum Privilege {
    Insert,
    Update,
    Read,
    Delete,
}

impl Privilege {
    fn as_byte(self) -> u8 {
        match self {
            Privilege::Insert => b'i',
            Privilege::Update => b'u',
            Privilege::Read => b'r',
            Privilege::Delete => b'd',
        }
    }
}

impl PrivilegeManager {
    /// Grants a privilege (`what`) by a user (`who`) to a user (`whom`) on an object (`on`).
    pub fn grant(
        &mut self,
        who: &AuthenticatedRole,
        whom: &UnauthenticatedRole,
        what: Privilege,
        on: Object,
    ) -> Result<&Grant, SqlPeError> {
        // `who` should sign the grant, `whom` should receive the necessary attribute keys.
        let mut grant = Grant {
            whom: whom.identity_key.ok_or(SqlPeError::MissingIdentityKey {
                role: whom.name.clone(),
            })?,
            what,
            on,

            signature: TrustRoot::NoTrust,
        };
        grant.sign(who)?;
        self.grants.push(grant);
        Ok(self.grants.last().expect("inserted grant"))
    }

    pub fn iter(&self) -> &[Grant] {
        &self.grants
    }

    pub fn can(&self, who: &UnauthenticatedRole, what: Privilege, on: Object) -> bool {
        let identity = who.identity_key.expect("grantee has an identity key");
        self.grants
            .iter()
            .any(|grant| grant.whom == identity && grant.implies(&what, &on))
    }
}

#[cfg(test)]
mod tests {
    use crate::roles::*;

    use super::*;

    #[test]
    fn alice_family_pictures_role_test() {
        let mut rm = RoleManager::default();
        let mut gm = PrivilegeManager::default();

        rm.create_role("root").expect("create root");
        rm.create_role("alice").expect("create alice");

        rm.set_authentication_key("root", &[1u8; 32])
            .expect("root is not currently authenticated");
        rm.set_authentication_key("alice", &[2u8; 32])
            .expect("alice is not yet authenticated");

        let root = rm
            .authenticated_by_name("root")
            .expect("root is authenticated");
        let alice = rm.by_name("alice").expect("get alice");

        let grant = gm
            .grant(
                root,
                alice,
                Privilege::Insert,
                Object::Table {
                    name: "pictures".into(),
                },
            )
            .expect("signed grant");

        assert!(grant
            .verify_trust(
                &rm.by_name("root")
                    .expect("root")
                    .identity_key
                    .as_ref()
                    .expect("root has an identity")
                    .decompress()
                    .expect("valid identity")
            )
            .expect("signature"));
        assert_eq!(grant.who().expect("signed grant"), root.identity_key());
    }

    #[test]
    fn sql_grant_object_implications() {
        let implications = [
            (
                // Table insert implies column insert
                (
                    Privilege::Insert,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
                (
                    Privilege::Insert,
                    Object::Column {
                        table: "pictures".into(),
                        column: "foo".into(),
                    },
                ),
            ),
            (
                // Table read implies column read
                (
                    Privilege::Read,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
                (
                    Privilege::Read,
                    Object::Column {
                        table: "pictures".into(),
                        column: "foo".into(),
                    },
                ),
            ),
        ];

        let tautologies = implications
            .clone()
            .into_iter()
            .flat_map(|((p1, o1), (p2, o2))| {
                [((p1, o1.clone()), (p1, o1)), ((p2, o2.clone()), (p2, o2))]
            });

        for (grant, check) in implications.into_iter().chain(tautologies) {
            let mut rm = RoleManager::default();
            let mut pm = PrivilegeManager::default();

            rm.create_role("root").expect("create root");
            rm.create_role("alice").expect("create alice");

            rm.set_authentication_key("root", &[1u8; 32])
                .expect("root is not currently authenticated");
            rm.set_authentication_key("alice", &[2u8; 32])
                .expect("alice is not yet authenticated");

            let root = rm
                .authenticated_by_name("root")
                .expect("root is authenticated");
            let alice = rm.by_name("alice").expect("get alice");

            let (privilege, object) = grant;
            let _grant = pm
                .grant(root, alice, privilege, object)
                .expect("signed grant");

            let (privilege, object) = check;
            assert!(pm.can(alice, privilege, object));
        }
    }

    #[test]
    fn sql_grant_object_invalid_implications() {
        let non_implications = [
            (
                (
                    Privilege::Delete,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
                (
                    Privilege::Insert,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
            ),
            (
                (
                    Privilege::Update,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
                (
                    Privilege::Insert,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
            ),
            (
                (
                    Privilege::Read,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
                (
                    Privilege::Insert,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
            ),
            (
                (
                    Privilege::Insert,
                    Object::Column {
                        table: "pictures".into(),
                        column: "foo".into(),
                    },
                ),
                (
                    Privilege::Insert,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
            ),
            (
                (
                    Privilege::Read,
                    Object::Column {
                        table: "pictures".into(),
                        column: "foo".into(),
                    },
                ),
                (
                    Privilege::Read,
                    Object::Table {
                        name: "pictures".into(),
                    },
                ),
            ),
        ];

        for (grant, check) in non_implications.into_iter() {
            let mut rm = RoleManager::default();
            let mut pm = PrivilegeManager::default();

            rm.create_role("root").expect("create root");
            rm.create_role("alice").expect("create alice");

            rm.set_authentication_key("root", &[1u8; 32])
                .expect("root is not currently authenticated");
            rm.set_authentication_key("alice", &[2u8; 32])
                .expect("alice is not yet authenticated");

            let root = rm
                .authenticated_by_name("root")
                .expect("root is authenticated");
            let alice = rm.by_name("alice").expect("get alice");

            let (privilege, object) = grant;
            let _grant = pm
                .grant(root, alice, privilege, object)
                .expect("signed grant");

            let (privilege, object) = check;
            assert!(!pm.can(alice, privilege, object));
        }
    }
}
